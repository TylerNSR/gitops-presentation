
<!--slide-attr x=0 y=0 rotate=0 scale=2 -->
![GitOps](media/zak-sakata-1346019-unsplash-edit.png)

>
>## Git·Ops
>### */ɡit,äps/*
>*noun* INFORMAL 
>
>IT operations using *git* to manage change across infrastructure and development 
>
<!-- >"GitOps my lawn" -->

presentation by *Tyler Sparks*

------
<!--slide-attr x=-800 y=1000 -->
## Why GitOps
Success for most companies and engineering groups is based <br /> on the interactions of a large, complex, distributed system


<!-- TODO: 
- Hashi "Shift Left" concept
- Infrastructure and GIT concepts -->
------
<!--slide-attr x=800 y=1000 -->
## What is it
Core Concepts
- environments as code
- change by merge request/pull request
    
*Coined by [WeaveWorks](https://www.weave.works/blog/gitops-operations-by-pull-request)*

------
<!--slide-attr x=-800 y=2000-->
## Operating Model
- GIT as the *single* source of truth for all resources/teams
- GIT is the *only* place where changes are made
- Automation is driven by making changes in GIT

**EVERYTHING AS CODE**

------
<!--slide-attr x=800 y=2000 -->
## What do I get?
- Reduced complexity of work and processes
- Increased contextual observability
- Proper coupling of system components

------
<style>
    div#step-6 {opacity: 1; text-align: center;}
</style>
<!--slide-attr x=2200 y=1000 scale=2 -->
<img src="media/workflow.svg" style="opacity: 1 !important; height: 100vh;">
<!-- ![Branches](media/workflow.svg) -->

------
<!--slide-attr x=2150 y=1350 rotate=-24 scale=1 -->

------
<!--slide-attr x=2450 y=1500 rotate=0 scale=1 -->

------
<!--slide-attr x=2450 y=1350 rotate=0 scale=1 -->

------
<!--slide-attr x=2450 y=950 rotate=0 scale=1 -->

------
<style>
    div#step-11 {opacity: 1; text-align: center;}
</style>
<!--slide-attr x=3600 y=1150 rotate=0 scale=0.6 -->
<img src="media/merge_request.png" style="opacity: 1 !important;">

------
<!--slide-attr x=3600 y=900 rotate=0 scale=0.4 -->

------
<!--slide-attr x=3600 y=1100 rotate=0 scale=0.4 -->


------
<!--slide-attr x=2450 y=750 rotate=0 scale=1 -->

------
<!--slide-attr x=3600 y=1300 rotate=0 scale=0.4 -->

------
<!--slide-attr x=2150 y=450 rotate=-24 scale=1 -->

------
<!--slide-attr x=2450 y=450 rotate=0 scale=1 -->

------
<style>div#step-1 {opacity: 1;}</style>
<!--slide-attr x=1000 y=1000 rotate=0 scale=6 -->

------
<!--slide-attr x=0 y=2600 -->
## References

1. Title Photo by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@zaks?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Zak Sakata"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Zak Sakata</span></a> on Unsplash 
2. Weaveworks (https://www.weave.works/blog/gitops-operations-by-pull-request)
3. Google GitOps Demo (https://www.youtube.com/watch?v=XL9CQobFB8I)
4. Merge Requests (https://docs.gitlab.com/ee/user/project/merge_requests/)
5. Review Apps (https://docs.gitlab.com/ee/ci/review_apps/)